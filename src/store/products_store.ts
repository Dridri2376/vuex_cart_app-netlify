import Product from "@/models/Product";
import { db } from '../db/fakeProductsDB';
import { cartStore } from './cart_store';
import { checkIfDuplicateExists } from '../helpers';
import displayedDetails from '../observables/Details'

export const productsStore = {
    state: {
        products: Array<Product>(),
        alreadyKnowProducts: Array<Product>(),
        details: {}
    },
    getters: {
        allProducts: (state) => state.products,
        detailedProduct: (state) => state.details,

    },
    actions: {
        GetAllProducts: async ({ commit }): Promise<any> => {
            try
            {
                const listOfProducts = db;
                commit("LISTOFALL", listOfProducts);
            } catch (error)
            {
                throw error;
            }
        },
        addProductToCart: async ({ commit }, product): Promise<any> => {
            try
            {
                commit("ADDONE", product);
            } catch (error)
            {
                throw error;
            }
        },
        getDetailsOfProduct: async ({ commit }, product): Promise<any> => {
            try
            {
                commit("PRODUCTDETAILS", product);
            } catch (error)
            {
                throw error;
            }
        },
    },
    mutations: {
        LISTOFALL: (state, products: Product[]) => {
            return state.products = products;
        },
        ADDONE: (state, product) => {
            const selectedProduct = state.products.find((po: any) => po === product);
            if (selectedProduct && selectedProduct !== null)
            {
                //add selectedProduct info into array
                selectedProduct.quantity = selectedProduct.quantity || 1; // important
                cartStore.state.cart.push(selectedProduct);
                cartStore.state.incrementPriceValue += selectedProduct.price;

                //add selectedProduct id into duplicateArray
                state.alreadyKnowProducts.push(selectedProduct.id);

                //check if duplicate
                if (state.alreadyKnowProducts.length > 1)
                {
                    const duplicate = checkIfDuplicateExists(state.alreadyKnowProducts);
                    if (duplicate)
                    {
                        selectedProduct.quantity++;
                        state.alreadyKnowProducts.pop();
                        cartStore.state.cart.pop();
                    }
                }
            }
        },
        PRODUCTDETAILS: (state, product) => {
            product = state.products.find((po: any) => po === product);
            /* fill state.details object */
            state.details = product;
            /* activate vue observable */
            displayedDetails.details = true;
            return state.details;
        }
    },
};


