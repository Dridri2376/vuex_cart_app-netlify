export const db = [{
    id: 1,
    title: "PS5",
    price: 599,
    category: "Video Games",
    description: "lastGen consoles",
    quantity: 1,
    image_url: require("../assets/imgs/ps5.jpg"),
    details_url: "https://www.playstation.com/fr-fr/ps5"
},
{
    id: 2,
    title: "XBOX",
    price: 499,
    category: "Video Games",
    description: "lastGen consoles",
    quantity: 1,
    image_url: require("../assets/imgs/XBOX.jpg"),
    details_url: "https://www.xbox.com/fr-FR/consoles/xbox-series-x"
},
{
    id: 3,
    title: "SWITCH",
    price: 260,
    category: "Video Games",
    description: "lastGen consoles",
    quantity: 1,
    image_url: require("../assets/imgs/switch.jpg"),
    details_url: "https://www.nintendo.fr/Gamme-Nintendo-Switch/Nintendo-Switch/Nintendo-Switch-1148779.html"
},
{
    id: 4,
    title: "PC",
    price: 1299,
    category: "Computer and Electronics",
    description: "gaming computer",
    quantity: 1,
    image_url: require("../assets/imgs/tower.jpg"),
    details_url: "https://www.lenovo.com/fr/fr"
},
{
    id: 5,
    title: "Display",
    price: 199,
    category: "gaming accessories",
    description: "LED display",
    quantity: 1,
    image_url: require("../assets/imgs/iiyama.jpg"),
    details_url: "https://iiyama.com/fr_fr/"
},
{
    id: 6,
    title: "Keyboard",
    price: 89,
    category: "gaming accessories",
    description: "LED gaming keyboard",
    quantity: 1,
    image_url: require("../assets/imgs/keyboard.jpg"),
    details_url: "https://www.logitechg.com/fr-fr/products/gaming-keyboards.html"
}
];
