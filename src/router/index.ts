import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import ProductsView from '../views/ProductsView.vue';
import CartView from '../views/CartView.vue';


Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'ProductsView',
    component: ProductsView,
  },
  {
    path: '/cartvalidation',
    name: 'CartView',
    component: CartView,
  },

];

const router = new VueRouter({
  routes,
});

export default router;
